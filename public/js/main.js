// import anime from 'animejs/lib/anime'
let centSavingsInstance = null;

function iterCallback(instance) {
  console.log(`Iteration: ${instance.i}`);
  updateCurrentCostInput(instance.costs[instance.i]);
  updateProgressBar(instance.i, instance.n);
  // clearTotalCostsContainer();
  setTotalCostsElements(instance.total_costs, instance.i);
  if (instance.i === instance.n) {
    logToConsole(`Result with n=${instance.n} and d=${instance.d} is ${instance.result}.`)
  }
}

function onTotalCostsChange(totalCosts, changedPos) {
  console.log(`OnTotalCostsChange: ${totalCosts}`)
}

function logToConsole(txt = '') {
  const row = document.createElement('span');
  row.innerHTML = txt;
  document.getElementById('log-container').appendChild(row)
}


// Check for the various File API support.
if (window.File && window.FileReader && window.FileList && window.Blob) {
  // Great success! All the File APIs are supported.
} else {
  alert('The File APIs are not fully supported in this browser.');
}


function handleFileSelect(evt) {
  const file = evt[0];
  const reader = new FileReader();
  reader.onload = (function (theFile) {
    return function (e) {
      const res = e.target.result;
      const match = res.match(/^(\d+)\s(\d+)((?:\d|\s)+)$/);
      const n = parseInt(match[1]);
      const d = parseInt(match[2]);
      const costs = match[3].split(' ').map(Number);
      updateModelInputs(n, d, costs)
    };
  })(file);
  reader.readAsText(file);
}

function updateCurrentCostInput(cost = 0) {
  document.getElementById('current-cost').value = cost;
  document.getElementById('cashier-amount').innerHTML = cost;
}

function updateModelInputs(n, d, costs) {
  document.getElementById("number-costs").value = n;
  document.getElementById("number-dividers").value = d;
  document.getElementById("list-costs").value = costs.join(' ');
}


function updateProgressBar(i, n) {
  const progressBar = document.getElementById('algo-progress-bar');
  const curIter = document.getElementById('cur-iter');
  const maxIter = document.getElementById('max-iter');
  const progress = (i) / n * 100;
  progressBar.style.width = progress + '%';
  curIter.innerText = i;
  maxIter.innerText = n;
}

function clearTotalCostsContainer() {
  const container = document.getElementById('support-sim-info-container');
  container.innerHTML = '';
}

function setTotalCostsElements(totalCosts = [], iter=0) {
  const container = document.getElementById('support-sim-info-container');
  const rowDiv = document.createElement('div');
  rowDiv.setAttribute('class', 'total_cost_row');
  let entry = document.createElement('div');
  entry.setAttribute('class', 'total-costs-entry');
  entry.innerHTML = `cost no. ${iter}`;
  rowDiv.appendChild(entry);
  for (let i = 0; i < totalCosts.length; i++) {
    // <div class="total-costs-entry">
    //     <div class="total-costs-value">23</div>
    //     <div class="total-costs-index">2</div>
    // </div>
    entry = document.createElement('div');
    entry.setAttribute('id', `total-cost-${i}`);
    entry.setAttribute('class', 'total-costs-entry');
    const value = document.createElement('div');
    value.setAttribute('id', `total-cost-value-${i}`);
    value.setAttribute('class', 'total-costs-value');
    value.innerHTML = totalCosts[i];
    const index = document.createElement('div');
    index.setAttribute('id', `total-cost-index-${i}`);
    index.setAttribute('class', 'total-costs-index');
    index.innerHTML = `${i} dividers`;
    entry.appendChild(value);
    entry.appendChild(index);
    rowDiv.appendChild(entry);
  }
  container.prepend(rowDiv);
  if (container.childElementCount>2){
    for (let j = 2; j < container.childElementCount; j++) {
      container.children.item(j).setAttribute("class", "total_cost_row old_row");
    }
  }
}

function updateTotalCostsElements(totalCosts = []) {

}


function onPlayAction() {
  console.log('PlayAction');
  if (centSavingsInstance == null) {
    onStopAction()
  }
  if (centSavingsInstance != null) {
    centSavingsInstance.play = true;
  }
}

function onPauseAction() {
  console.log('PauseAction');
  if (centSavingsInstance != null) {
    centSavingsInstance.play = false;
  }
}

function onStopAction() {
  console.log('StopAction');
  logToConsole('Cleared');
  centSavingsInstance = new CentSavings(
    document.getElementById("number-costs").value,
    document.getElementById("number-dividers").value,
    document.getElementById("list-costs").value.split(' ').map(Number)
  );
  console.log(`New instance: ${centSavingsInstance}`);
  centSavingsInstance.iterCallback = iterCallback;
  centSavingsInstance.onTotalCostsChange = onTotalCostsChange;
  updateProgressBar(centSavingsInstance.i, centSavingsInstance.n);
  clearTotalCostsContainer();
  centSavingsInstance.iterCallback(centSavingsInstance);
  // setTotalCostsElements(centSavingsInstance.total_costs);
  logToConsole('Initialized')
}

function onOneStepAction() {
  console.log('StepAction')
}

function onOneIterAction() {
  logToConsole('+1 Iteration');
  centSavingsInstance.iter();
}

class CentSavings {
  constructor(n, d, costs) {
    this.n = parseInt(n);
    this.d = parseInt(d);
    this.costs = costs;
    // this.callback = callback;
    this.total_costs = [0];
    this.result = 0;
    this.i = 0;
    for (this.i = 0; this.i < this.d; this.i++) {
      this.total_costs.push(1000000000);
    }
    this.i = 0;
    this.iterCallback = null;
    this.onTotalCostsChange = null;
  }

  // pause() {
  //   return new Promise(
  //     resolve => {
  //       do {
  //         console.log('pause');
  //         setTimeout(this.pause, 100);
  //       } while ((!this.play));
  //       return resolve('resolved');
  //     }
  //   );
  // }

  // async run() {
  //   for (this.i = 0; this.i < this.n; this.i++) {
  //     this.iter()
  //   }
  // }

  iter() {
    if (this.i < this.n) {
      this.total_costs[0] += this.costs[this.i];
      this.onTotalCostsChange(this.total_costs, 0);
      for (let j = 1; j < this.d + 1; j++) {
        const roundVal = parseInt((this.total_costs[j - 1] + 5) / 10) * 10;
        this.total_costs[j] = Math.min(this.total_costs[j] + this.costs[this.i], roundVal);
        this.onTotalCostsChange(this.total_costs, j);
      }
      this.i++;
    }
    const retval = parseInt((this.total_costs[this.d] + 5) / 10) * 10;
    console.log(`Result: ${retval}`);
    this.result = retval;
    this.iterCallback(this);
    return retval;
  }
}

// var controlsProgressEl = document.querySelector('.timeline-controls-demo .progress');
//
// var tl = anime.timeline({
//   direction: 'alternate',
//   loop: true,
//   duration: 500,
//   easing: 'easeInOutSine',
//   update: function(anim) {
//     controlsProgressEl.value = tl.progress;
//   }
// });
//
// tl
//   .add({
//     targets: '.timeline-controls-demo .square.el',
//     translateX: 270,
//   })
//   .add({
//     targets: '.timeline-controls-demo .circle.el',
//     translateX: 270,
//   }, '-=100')
//   .add({
//     targets: '.timeline-controls-demo .triangle.el',
//     translateX: 270,
//   }, '-=100');
//
// document.querySelector('.timeline-controls-demo .play').onclick = tl.play;
// document.querySelector('.timeline-controls-demo .pause').onclick = tl.pause;
// document.querySelector('.timeline-controls-demo .restart').onclick = tl.restart;
//
// controlsProgressEl.addEventListener('input', function() {
//   tl.seek(tl.duration * (controlsProgressEl.value / 100));
// });
function multiple(x, y) {
  result = 0;
  for (i = x; i > 0; i--) {
    result = result + y;
    if (result > 100) {
      console.log()
    } else {

    }
  }
  console.log(result);
}
